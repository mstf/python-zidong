import pyautogui
import time
import xlrd
import pyperclip

#定义鼠标事件

#pyautogui库其他用法 https://blog.csdn.net/qingfengxd1/article/details/108270159

def mouseClick(clickTimes,lOrR,img,reTry):
    if reTry == 1:
        while True:
            location=pyautogui.locateCenterOnScreen(img,confidence=0.9)
            if location is not None:
                pyautogui.click(location.x,location.y,clicks=clickTimes,interval=0.2,duration=0.2,button=lOrR)
                break
            print("未找到匹配图片,0.1秒后重试")
            time.sleep(0.1)
    elif reTry == -1:
        while True:
            location=pyautogui.locateCenterOnScreen(img,confidence=0.9)
            if location is not None:
                pyautogui.click(location.x,location.y,clicks=clickTimes,interval=0.2,duration=0.2,button=lOrR)
            time.sleep(0.1)
    elif reTry > 1:
        i = 1
        while i < reTry + 1:
            location=pyautogui.locateCenterOnScreen(img,confidence=0.9)
            if location is not None:
                pyautogui.click(location.x,location.y,clicks=clickTimes,interval=0.2,duration=0.2,button=lOrR)
                print("重复")
                i += 1
            time.sleep(0.1)

# 数据检查
# cmdType.value  1.0左键单击   1.1左键双击  1.2代表移动鼠标光标到指定坐标  1.3按下鼠标左键拖动到指定坐标
#                2.0右键单击  
#                3.0滚轮  
#                4.0输入  
#                5.0等待  
#                6.0模拟按键  6.1按键按下  6.2按键松开  6.3两按键组合  6.4三按键组合
#                7.0区域截图到指定位置 7.1全屏截图到指定位置
#                8.0弹窗提示

# ctype     空：0
#           字符串：1
#           数字：2
#           日期：3
#           布尔：4
#           error：5
def dataCheck(sheet1):
    checkCmd = True
    #行数检查
    if sheet1.nrows<2:
        print("没数据啊哥")
        checkCmd = False
    #每行数据检查
    i = 1
    while i < sheet1.nrows:
        # 第1列 操作类型检查
        cmdType = sheet1.row(i)[0]
        if cmdType.ctype != 2 or (cmdType.value != 1.0 and cmdType.value != 1.1 and cmdType.value != 1.2 and cmdType.value != 1.3
        and cmdType.value != 2.0 
        and cmdType.value != 3.0
        and cmdType.value != 4.0 
        and cmdType.value != 5.0 
        and cmdType.value != 6.0 and cmdType.value != 6.1 and cmdType.value != 6.2 and cmdType.value != 6.3 and cmdType.value != 6.4 
        and cmdType.value != 7.0 and cmdType.value != 7.1
        and cmdType.value != 8 ):
            print('第',i+1,"行,第1列数据有毛病")
            checkCmd = False
        # 第2列 内容检查
        cmdValue = sheet1.row(i)[1]
        # 读图点击类型指令，内容必须为字符串类型
        if cmdType.value ==1.0 or cmdType.value == 1.1 or cmdType.value == 2.0:
            if cmdValue.ctype != 1:
                print('第',i+1,"行,第2列数据有毛病")
                checkCmd = False
        if cmdType.value == 1.2 or cmdType.value == 1.3:
            if cmdValue.ctype != 2:
                print('第',i+1,"行,第2列数据有毛病")
                checkCmd = False
        # 3滚轮事件，内容必须为数字
        if cmdType.value == 6.0:
            if cmdValue.ctype == 0:
                print('第',i+1,"行,第2列数据有毛病")
                checkCmd = False
        # 4输入类型，内容不能为空
        if cmdType.value == 4.0:
            if cmdValue.ctype == 0:
                print('第',i+1,"行,第2列数据有毛病")
                checkCmd = False
        # 5等待类型，内容必须为数字
        if cmdType.value == 5.0:
            if cmdValue.ctype != 2:
                print('第',i+1,"行,第2列数据有毛病")
                checkCmd = False
        # 6按键事件，内容不能为空
        if cmdType.value == 6.0 or cmdType.value == 6.1 or cmdType.value == 6.2 or cmdType.value == 6.3 or cmdType.value == 6.4:
            if cmdValue.ctype == 0:
                print('第',i+1,"行,第2列数据有毛病")
                checkCmd = False
        # 7截图事件，内容必须为数字
        if cmdType.value == 7.0:
            if cmdValue.ctype != 2:
                print('第',i+1,"行,第2列数据有毛病")
                checkCmd = False
        # 7.1全屏截图事件，内容必须为空
        if cmdType.value == 7.1:
            if cmdValue.ctype != 0:
                print('第',i+1,"行,第2列数据有毛病")
                checkCmd = False
        # 8弹窗事件，内容不能为空
        if cmdType.value == 8.0:
            if cmdValue.ctype == 0:
                print('第',i+1,"行,第2列数据有毛病")
                checkCmd = False
        i += 1
    return checkCmd

#任务
def mainWork(img):
    i = 1
    while i < sheet1.nrows:
        #取本行指令的操作类型
        cmdType = sheet1.row(i)[0]
        #1.0代表单击左键
        if cmdType.value == 1.0:
            #取图片名称
            img = sheet1.row(i)[1].value
            reTry = 1
            if sheet1.row(i)[2].ctype == 2 and sheet1.row(i)[2].value != 0:
                reTry = sheet1.row(i)[2].value
            mouseClick(1,"left",img,reTry)
            print("单击左键",img)
        #1.1代表双击左键
        elif cmdType.value == 1.1:
            #取图片名称
            img = sheet1.row(i)[1].value
            #取重试次数
            reTry = 1
            if sheet1.row(i)[2].ctype == 2 and sheet1.row(i)[2].value != 0:
                reTry = sheet1.row(i)[2].value
            mouseClick(2,"left",img,reTry)
            print("双击左键",img)
        #1.2代表移动鼠标光标到指定坐标
        elif cmdType.value == 1.2:
            x1 = sheet1.row(i)[1].value
            y1 = sheet1.row(i)[3].value
            pyautogui.moveTo(x1,y1, duration=0.25)
            time.sleep(0.5)
            print("坐标:",x1,y1)
        #1.3代表按下鼠标左键拖动光标到指定坐标
        elif cmdType.value == 1.3:
            x1 = sheet1.row(i)[1].value
            y1 = sheet1.row(i)[3].value
            pyautogui.mouseDown()  # 按下鼠标左键
            pyautogui.moveTo(x1,y1, duration=0.25) # 移动到指定位置
            pyautogui.mouseUp()  # 松开鼠标左键
            time.sleep(0.5)
            print("坐标:",x1,y1)
        #2.0代表右键单机
        elif cmdType.value == 2.0:
            #取图片名称
            img = sheet1.row(i)[1].value
            #取重试次数
            reTry = 1
            if sheet1.row(i)[2].ctype == 2 and sheet1.row(i)[2].value != 0:
                reTry = sheet1.row(i)[2].value
            mouseClick(1,"right",img,reTry)
            print("右键",img)
        #3代表滚轮
        elif cmdType.value == 3.0:
            #取图片名称
            scroll = sheet1.row(i)[1].value
            pyautogui.scroll(int(scroll))
            print("滚轮滑动",int(scroll),"距离")
        #4代表输入
        elif cmdType.value == 4.0:
            inputValue = sheet1.row(i)[1].value
            pyperclip.copy(inputValue)
            pyautogui.hotkey('ctrl','v')
            time.sleep(0.5)
            print("输入:",inputValue)
        #5代表等待
        elif cmdType.value == 5.0:
            waitTime = sheet1.row(i)[1].value
            time.sleep(waitTime)
            print("等待",waitTime,"秒")
        #6代表模拟单按键
        elif cmdType.value == 6.0:
            key0 = sheet1.row(i)[1].value
            pyautogui.press(key0)
            time.sleep(0.5)
            print("按键模拟:",key0)
        #6.1代表模拟单按键
        elif cmdType.value == 6.1:
            key0 = sheet1.row(i)[1].value
            pyautogui.keyDown(key0)
            time.sleep(0.5)
            print("按下:",key0)
        #6.2代表模拟单按键
        elif cmdType.value == 6.2:
            key0 = sheet1.row(i)[1].value
            pyautogui.keyUp(key0)
            time.sleep(0.5)
            print("松开:",key0)
        #6.3代表模拟二组按键
        elif cmdType.value == 6.3:
            key1 = sheet1.row(i)[1].value
            key2 = sheet1.row(i)[3].value
            pyautogui.hotkey(key1,key2)
            time.sleep(0.5)
            print("按键组合:",key1 + key2)
        #6.4代表模拟三组按键
        elif cmdType.value == 6.4:
            key1 = sheet1.row(i)[1].value
            key2 = sheet1.row(i)[3].value
            key3 = sheet1.row(i)[4].value
            pyautogui.hotkey(key1,key2,key3)
            time.sleep(0.5)
            print("三组按键组合:",key1 + key2 + key3)
        #7.0代表区域截图
        elif cmdType.value == 7.0:
            key1 = sheet1.row(i)[1].value
            key2 = sheet1.row(i)[3].value
            key3 = sheet1.row(i)[4].value
            key4 = sheet1.row(i)[5].value
            pyautogui.screenshot(r'C:\py0.png', region=(key1, key2, key3, key4))
            time.sleep(0.5)
            print("区域截图已经保存到C:\py0.png")
        #7.1代表全屏截图
        elif cmdType.value == 7.1:
            pyautogui.screenshot(r'C:\py1.png')
            time.sleep(0.5)
            print("全屏截图已经保存到C:\py1.png")
        #8.0代表弹窗提示
        elif cmdType.value == 8.0:
            key1 = sheet1.row(i)[1].value
            key2 = sheet1.row(i)[3].value
            pyautogui.alert(text=key1, title=key2, button='OK')
            time.sleep(0.5)
            print("弹窗提示已经确认")
            
        i += 1

if __name__ == '__main__':
    file = 'cmd.xls'
    #打开文件
    wb = xlrd.open_workbook(filename=file)
    #通过索引获取表格sheet页
    sheet1 = wb.sheet_by_index(0)
    print('python自动化办公模拟操作源码（源码基于“不高兴就喝水”，墨涩加强版2023.1.6）')
    #数据检查
    checkCmd = dataCheck(sheet1)
    if checkCmd:
       # key=input ('选择功能: 1.做一次 2.循环到死 \n')
        #if key=='1':
            #循环拿出每一行指令
            mainWork(sheet1)
       # elif key=='2':
           # while True:
               # mainWork(sheet1)
               # time.sleep(0.1)
               # print("等待0.1秒")
   # else:
       # print('输入有误或者已经退出!')
